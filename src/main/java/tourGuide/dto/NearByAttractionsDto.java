package tourGuide.dto;

import tourGuide.proxy.gps.LocationProxy;

public class NearByAttractionsDto {
  /*
   * Renvoie un nouvel objet JSON qui contient :
   *   // Nom de l'attraction touristique,
   *   // Attraits touristiques lat/long,
   *   // La position lat/long de l'utilisateur,
   *   // La distance en miles entre l'emplacement de l'utilisateur et chacune des attractions.
   *   // Les points de récompense pour visiter chaque attraction.
   *   // Remarque : les points de récompense d'attraction peuvent être collectés auprès de RewardsCentral*/

  private String attractionName;

  private LocationProxy attractionLocation;

  private LocationProxy userLocation;

  private Double distance;

  private int rewardPointValue;

  public NearByAttractionsDto(String attractionName, LocationProxy attractionLocation, LocationProxy userLocation, double distance, int rewardPointValue) {
    this.attractionName = attractionName;
    this.attractionLocation = attractionLocation;
    this.userLocation = userLocation;
    this.distance = distance;
    this.rewardPointValue = rewardPointValue;
  }

  public String getAttractionName() {
    return attractionName;
  }

  public void setAttractionName(String attractionName) {
    this.attractionName = attractionName;
  }

  public LocationProxy getAttractionLocation() {
    return attractionLocation;
  }

  public void setAttractionLocation(LocationProxy attractionLocation) {
    this.attractionLocation = attractionLocation;
  }

  public LocationProxy getUserLocation() {
    return userLocation;
  }

  public void setUserLocation(LocationProxy userLocation) {
    this.userLocation = userLocation;
  }

  public double getDistance() {
    return distance;
  }

  public void setDistance(double distance) {
    this.distance = distance;
  }

  public int getRewardPointValue() {
    return rewardPointValue;
  }

  public void setRewardPointValue(int rewardPointValue) {
    this.rewardPointValue = rewardPointValue;
  }
}
