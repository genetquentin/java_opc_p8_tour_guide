package tourGuide.dto;

import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.math.BigDecimal;

public class UserSetterPreferencesDto {

  private int attractionProximity = Integer.MAX_VALUE;

  private CurrencyUnit currency = Monetary.getCurrency("USD");

  private int lowerPricePoint;

  private int highPricePoint = Integer.MAX_VALUE;

  private int tripDuration = 1;

  private int ticketQuantity = 1;

  private int numberOfAdults = 1;

  private int numberOfChildren = 0;

  public int getAttractionProximity() {
    return attractionProximity;
  }

  public void setAttractionProximity(int attractionProximity) {
    this.attractionProximity = attractionProximity;
  }

  public CurrencyUnit getCurrency() {
    return currency;
  }

  public void setCurrency(CurrencyUnit currency) {
    this.currency = currency;
  }

  public int getLowerPricePoint() {
    return lowerPricePoint;
  }

  public void setLowerPricePoint(int lowerPricePoint) {
    this.lowerPricePoint = lowerPricePoint;
  }

  public int getHighPricePoint() {
    return highPricePoint;
  }

  public void setHighPricePoint(int highPricePoint) {
    this.highPricePoint = highPricePoint;
  }

  public int getTripDuration() {
    return tripDuration;
  }

  public void setTripDuration(int tripDuration) {
    this.tripDuration = tripDuration;
  }

  public int getTicketQuantity() {
    return ticketQuantity;
  }

  public void setTicketQuantity(int ticketQuantity) {
    this.ticketQuantity = ticketQuantity;
  }

  public int getNumberOfAdults() {
    return numberOfAdults;
  }

  public void setNumberOfAdults(int numberOfAdults) {
    this.numberOfAdults = numberOfAdults;
  }

  public int getNumberOfChildren() {
    return numberOfChildren;
  }

  public void setNumberOfChildren(int numberOfChildren) {
    this.numberOfChildren = numberOfChildren;
  }
}
