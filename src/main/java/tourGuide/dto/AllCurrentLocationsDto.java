package tourGuide.dto;

import tourGuide.proxy.gps.LocationProxy;

public class AllCurrentLocationsDto {


  private String userId;

  private LocationProxy userLastLocation;

  public AllCurrentLocationsDto(String userId, LocationProxy userLastLocation) {
    this.userId = userId;
    this.userLastLocation = userLastLocation;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public LocationProxy getUserLastLocation() {
    return userLastLocation;
  }

  public void setUserLastLocation(LocationProxy userLastLocation) {
    this.userLastLocation = userLastLocation;
  }
}
