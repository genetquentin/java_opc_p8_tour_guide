package tourGuide.tracker;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tourGuide.service.TourGuideService;
import tourGuide.user.User;

import javax.xml.bind.SchemaOutputResolver;

public class Tracker extends Thread {

  private Logger logger = LoggerFactory.getLogger(Tracker.class);

  private static final long trackingPollingInterval = TimeUnit.SECONDS.toSeconds(10);

  private final ExecutorService executorService = Executors.newSingleThreadExecutor();

  private final TourGuideService tourGuideService;

  private boolean stop = false;

  public Tracker(TourGuideService tourGuideService) {
    this.tourGuideService = tourGuideService;
    executorService.submit(this);
  }

  /**
   * Assures to shut down the Tracker thread
   */
  public void stopTracking() {
    stop = true;
    executorService.shutdownNow();
  }

  @Override
  public void run() {

    // A chaque tour de boucle, un tracking tous les utilisateurs
    while (true) {
      if (Thread.currentThread().isInterrupted() || stop) {
        logger.debug("Tracker stopping");
        break;
      }
      List<User> users = tourGuideService.getAllUsers();
      new TrackingTask(tourGuideService).doTracking(users);
      try {
        logger.debug("Tracker sleeping");
        TimeUnit.SECONDS.sleep(trackingPollingInterval);
      } catch (InterruptedException e) {
        break;
      }
    }

  }
}
