package tourGuide.tracker;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tourGuide.service.TourGuideService;
import tourGuide.user.User;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TrackingTask {

  private Logger logger = LoggerFactory.getLogger(TrackingTask.class);

  private TourGuideService tourGuideService;

  public TrackingTask(TourGuideService tourGuideService) {
    this.tourGuideService = tourGuideService;

  }

  public void doTracking(List<User> users) {
    StopWatch stopWatch = new StopWatch();
    ExecutorService threadPool = Executors.newFixedThreadPool(200);
    logger.debug("Begin Tracker. Tracking " + users.size() + " users.");
    stopWatch.start();
    for (User u : users) {
      threadPool.submit(new Thread() {
        public void run() {
          tourGuideService.trackUserLocation(u);
        }
      });
    }
    threadPool.shutdown();
    try {
      threadPool.awaitTermination(15, TimeUnit.MINUTES);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    stopWatch.stop();
    logger.debug("Tracker Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
  }

}
