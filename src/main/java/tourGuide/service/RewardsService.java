package tourGuide.service;


import org.springframework.stereotype.Service;
import tourGuide.proxy.gps.AttractionProxy;
import tourGuide.proxy.gps.GpsProxy;
import tourGuide.proxy.gps.LocationProxy;
import tourGuide.proxy.gps.VisitedLocationProxy;
import tourGuide.proxy.rewardCentral.RewardCentralProxy;
import tourGuide.user.User;
import tourGuide.user.UserReward;

import java.util.List;
import java.util.UUID;

@Service
public class RewardsService {

  private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

  // proximity in miles
  private int defaultProximityBuffer = 10;

  private int proximityBuffer = defaultProximityBuffer;

  //by default attractionProximityRange is setted to 200
  private int attractionProximityRange = 5000;

  private final GpsProxy gpsProxy;

  private final RewardCentralProxy rewardsCentral;

  public RewardsService(GpsProxy gpsProxy, RewardCentralProxy rewardCentral) {
    this.gpsProxy = gpsProxy;
    this.rewardsCentral = rewardCentral;
  }

  public void setProximityBuffer(int proximityBuffer) {
    this.proximityBuffer = proximityBuffer;
  }

  public void setDefaultProximityBuffer() {
    proximityBuffer = defaultProximityBuffer;
  }

  public void calculateRewards(User user) {
    List<VisitedLocationProxy> userLocations = user.getVisitedLocations();
    List<AttractionProxy> attractions = gpsProxy.getAttractions();

    for (VisitedLocationProxy visitedLocation : userLocations) {
      for (AttractionProxy attraction : attractions) {
        if (user.getUserRewards().stream().filter(r -> r.attraction.attractionName.equals(attraction.attractionName)).count() == 0) {
          
          if (nearAttraction(visitedLocation, attraction)) {

            user.addUserReward(new UserReward(visitedLocation, attraction, getRewardPoints(attraction, visitedLocation.userId)));
          }
        }
      }
    }
  }

  //Faire un calculateAllReward avec threadpool + modifier les tests de performances sur reward central en simulant le nombre d'utilisateurs.

  
  public boolean isWithinAttractionProximity(AttractionProxy attraction, LocationProxy location) {
    return getDistance(attraction, location) > attractionProximityRange ? false : true;
  }

  private boolean nearAttraction(VisitedLocationProxy visitedLocation, AttractionProxy attraction) {
    return getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;
  }

  public int getRewardPoints(AttractionProxy attraction, UUID userId) {
    return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, userId);
  }

  public double getDistance(LocationProxy loc1, LocationProxy loc2) {
    double lat1 = Math.toRadians(loc1.latitude);
    double lon1 = Math.toRadians(loc1.longitude);
    double lat2 = Math.toRadians(loc2.latitude);
    double lon2 = Math.toRadians(loc2.longitude);

    double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
        + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

    double nauticalMiles = 60 * Math.toDegrees(angle);
    double statuteMiles = STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
    return statuteMiles;
  }

}
