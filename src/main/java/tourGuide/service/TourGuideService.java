package tourGuide.service;

import org.javamoney.moneta.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tourGuide.dto.NearByAttractionsDto;
import tourGuide.dto.UserSetterPreferencesDto;
import tourGuide.proxy.gps.AttractionProxy;
import tourGuide.proxy.gps.GpsProxy;
import tourGuide.proxy.gps.LocationProxy;
import tourGuide.proxy.gps.VisitedLocationProxy;
import tourGuide.proxy.tripPricer.ProviderProxy;
import tourGuide.proxy.tripPricer.TripPricerProxy;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;
import tourGuide.user.UserReward;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class TourGuideService {

  private Logger logger = LoggerFactory.getLogger(TourGuideService.class);

  private final GpsProxy gpsProxy;

  private final RewardsService rewardsService;

  private final TripPricerProxy tripPricer = new TripPricerProxy();

  //public final Tracker tracker;

  //boolean testMode = true;

  public TourGuideService(GpsProxy gpsProxy, RewardsService rewardsService) {
    this.gpsProxy = gpsProxy;
    this.rewardsService = rewardsService;
/*
    if (testMode) {
      logger.info("TestMode enabled");
      logger.debug("Initializing users");
      initializeInternalUsers();
      logger.debug("Finished initializing users");
    }

 */
    //addShutDownHook();
  }

  public List<UserReward> getUserRewards(User user) {
    return user.getUserRewards();
  }

  public VisitedLocationProxy getUserLocation(User user) {
    VisitedLocationProxy visitedLocation = (user.getVisitedLocations().size() > 0) ?
        user.getLastVisitedLocation() :
        trackUserLocation(user);
    return visitedLocation;
  }

  public User getUser(String userName) {
    return internalUserMap.get(userName);
  }

  public List<User> getAllUsers() {
    return internalUserMap.values().stream().collect(Collectors.toList());
  }

  public void addUser(User user) {
    if (!internalUserMap.containsKey(user.getUserName())) {
      internalUserMap.put(user.getUserName(), user);
    }
  }

  public List<ProviderProxy> getTripDeals(User user) {
    int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
    List<ProviderProxy> providers = tripPricer.getPrice(tripPricerApiKey, user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
        user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
    user.setTripDeals(providers);
    return providers;
  }

  public VisitedLocationProxy trackUserLocation(User user) {
    VisitedLocationProxy visitedLocation = gpsProxy.getUserLocation(user.getUserId());
    user.addToVisitedLocations(visitedLocation);
    rewardsService.calculateRewards(user);
    return visitedLocation;
  }

  /*
   * Stocker toutes les attractions touristiques visitées dans une ArrayList.
   * Faire un traitement de type stream sur cette arrayList globale
   * pour obtenir les 5 attractions le plus proches de l'utilisateur quelquesoit la distance avec la function 'getDistance' de reward service
   * et collecter ces infos dans une Array list de DTO contenant
   * des DTO spécifique à ce besoin, contenant les attributs désirés.
   * */
  /*
   *     On veut les 5 attractions les plus proches de l'utilisateur definies dans une liste de NearByAttractionDTO
   *     1 - Faire la liste de toutes les attractions
   *     2 - Noter la distance entre l'utilisateur et chaque attraction
   *     3 - Trier par distance
   *     4 - Garder que les 5 premiers
   */

  public List<NearByAttractionsDto> getNearByAttractions(VisitedLocationProxy visitedLocation) {

    List<AttractionProxy> nearbyAttractions = new ArrayList<>();
    List<NearByAttractionsDto> nearByAttractionsDtoList = new ArrayList<>();

    for (AttractionProxy attraction : gpsProxy.getAttractions()) {
      if (rewardsService.isWithinAttractionProximity(attraction, visitedLocation.location)) {
        nearbyAttractions.add(attraction);
      }
    }

      for (AttractionProxy attraction : nearbyAttractions) {
        NearByAttractionsDto nearByAttractionsDto = new NearByAttractionsDto(
            attraction.attractionName,
            attraction,
            visitedLocation.location,
            rewardsService.getDistance(attraction, visitedLocation.location),
            rewardsService.getRewardPoints(attraction, visitedLocation.userId)
        );
        nearByAttractionsDtoList.add(nearByAttractionsDto);
      }

      nearByAttractionsDtoList = nearByAttractionsDtoList.stream()
          .sorted(Comparator.comparingDouble(NearByAttractionsDto::getDistance))
          .limit(5)
          .collect(Collectors.toList());

    return nearByAttractionsDtoList;
  }

  //Current endpoint to implement getAllCurrentLocations
  public List<Map<String, LocationProxy>> getAllCurrentLocations() {

    List<User> userList = getAllUsers();
    List<Map<String, LocationProxy>> allCurrentLocationsDtoList = new ArrayList<>();
    userList.forEach(user -> {
      Map<String, LocationProxy> currentUserLocation = new HashMap<>();
      currentUserLocation.put(user.getUserId().toString(), user.getLastVisitedLocation().location);
      allCurrentLocationsDtoList.add(currentUserLocation);
    });
    return allCurrentLocationsDtoList;
  }

  public UserPreferences setUserPreferences(String userName, UserSetterPreferencesDto userSetterPreferencesDto) {
    User user = getUser(userName);
    UserPreferences userPreferences = new UserPreferences();
    if (user != null) {
      userPreferences.setAttractionProximity(userSetterPreferencesDto.getAttractionProximity());
      userPreferences.setHighPricePoint(Money.of(userSetterPreferencesDto.getHighPricePoint(),userSetterPreferencesDto.getCurrency()));
      userPreferences.setNumberOfChildren(userSetterPreferencesDto.getNumberOfChildren());
      userPreferences.setTicketQuantity(userSetterPreferencesDto.getTicketQuantity());
      userPreferences.setTripDuration(userSetterPreferencesDto.getTripDuration());
      userPreferences.setLowerPricePoint(Money.of(userSetterPreferencesDto.getLowerPricePoint(),userSetterPreferencesDto.getCurrency()));
      userPreferences.setNumberOfAdults(userSetterPreferencesDto.getNumberOfAdults());

      user.setUserPreferences(userPreferences);
    }
    return userPreferences;
  }

/*
  private void addShutDownHook() {
    Runtime.getRuntime().addShutdownHook(new Thread() {
      public void run() {
        tracker.stopTracking();
      }
    });
  }
*/
  /**********************************************************************************
   *
   * Methods Below: For Internal Testing
   *
   **********************************************************************************/
  private static final String tripPricerApiKey = "test-server-api-key";
  // Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
  private final Map<String, User> internalUserMap = new HashMap<>();

  public void initializeInternalUsers(int userNumber) {
    IntStream.range(0, userNumber).forEach(i -> {
      String userName = "internalUser" + i;
      String phone = "000";
      String email = userName + "@tourGuide.com";
      User user = new User(UUID.randomUUID(), userName, phone, email);
      generateUserLocationHistory(user);

      internalUserMap.put(userName, user);
    });
    logger.debug("Created " + userNumber + " internal test users.");
  }

  private void generateUserLocationHistory(User user) {
    IntStream.range(0, 3).forEach(i -> {
      user.addToVisitedLocations(new VisitedLocationProxy(user.getUserId(), new LocationProxy(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
    });
  }

  private double generateRandomLongitude() {
    double leftLimit = -180;
    double rightLimit = 180;
    return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
  }

  private double generateRandomLatitude() {
    double leftLimit = -85.05112878;
    double rightLimit = 85.05112878;
    return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
  }

  private Date getRandomTime() {
    LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
    return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
  }

}
