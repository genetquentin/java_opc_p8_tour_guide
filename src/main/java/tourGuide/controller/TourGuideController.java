package tourGuide.controller;

import com.jsoniter.output.JsonStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tourGuide.dto.UserSetterPreferencesDto;
import tourGuide.proxy.gps.VisitedLocationProxy;
import tourGuide.proxy.tripPricer.ProviderProxy;
import tourGuide.service.TourGuideService;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;

import java.util.List;

@RestController
public class TourGuideController {

  @Autowired
  TourGuideService tourGuideService;

  @RequestMapping("/")
  public String index() {
    return "Greetings from TourGuide!";
  }

  @RequestMapping("/getLocation")
  public String getLocation(@RequestParam String userName) {
    VisitedLocationProxy visitedLocation = tourGuideService.getUserLocation(getUser(userName));
    return JsonStream.serialize(visitedLocation.location);
  }

  //  TODO: Change this method to no longer return a List of Attractions.
  //  Instead: Get the closest five tourist attractions to the user - no matter how far away they are.
  //  Return a new JSON object that contains:
  // Name of Tourist attraction,
  // Tourist attractions lat/long,
  // The user's location lat/long,
  // The distance in miles between the user's location and each of the attractions.
  // The reward points for visiting each Attraction.
  //    Note: Attraction reward points can be gathered from RewardsCentral
  /*
  * // TODO : modifiez cette méthode pour ne plus renvoyer de liste d'attractions.
  // Au lieu de cela : obtenez les cinq attractions touristiques les plus proches de l'utilisateur, quelle que soit leur distance.
  // Renvoie un nouvel objet JSON qui contient :
  // Nom de l'attraction touristique,
  // Attraits touristiques lat/long,
  // La position lat/long de l'utilisateur,
  // La distance en miles entre l'emplacement de l'utilisateur et chacune des attractions.
  // Les points de récompense pour visiter chaque attraction.
  // Remarque : les points de récompense d'attraction peuvent être collectés auprès de RewardsCentral*/
  @RequestMapping("/getNearbyAttractions")
  public String getNearbyAttractions(@RequestParam String userName) {
    /*
    On veut les 5 attractions les plus proches de l'utilsateur definies dans une liste de NearByAttractionDTO
    1 - Faire la liste de toutes les attractions
    2 - Noter la distance entre l'utilisateur et chaque attraction
    3 - Trier par distance
    4 - Garder que les 5 premiers
     */
    VisitedLocationProxy visitedLocation = tourGuideService.getUserLocation(getUser(userName));
    return JsonStream.serialize(tourGuideService.getNearByAttractions(visitedLocation));
  }

  @RequestMapping("/getRewards")
  public String getRewards(@RequestParam String userName) {
    return JsonStream.serialize(tourGuideService.getUserRewards(getUser(userName)));
  }

  @RequestMapping("/getAllCurrentLocations")
  public String getAllCurrentLocations() {
    // TODO: Get a list of every user's most recent location as JSON
    //- Note: does not use gpsUtil to query for their current location,
    //        but rather gathers the user's current location from their stored location history.
    //
    // Return object should be the just a JSON mapping of userId to Locations similar to:
    //     {
    //        "019b04a9-067a-4c76-8817-ee75088c3822": {"longitude":-48.188821,"latitude":74.84371}
    //        ...
    //     }

    /*
    * À FAIRE : Obtenir une liste de l'emplacement le plus récent de chaque utilisateur au format JSON
    //- Remarque : n'utilise pas gpsUtil pour rechercher leur emplacement actuel,
    // mais rassemble plutôt l'emplacement actuel de l'utilisateur à partir de son historique de localisation stocké.
    //
    // L'objet de retour doit être simplement un mappage JSON de l'ID utilisateur aux emplacements similaires à :
    // {
    // "019b04a9-067a-4c76-8817-ee75088c3822": {"longitude":-48.188821,"latitude":74.84371}
    // ...
    // }
    * */
    return JsonStream.serialize(tourGuideService.getAllCurrentLocations());
  }

  @RequestMapping("/getTripDeals")
  public String getTripDeals(@RequestParam String userName) {
    List<ProviderProxy> providers = tourGuideService.getTripDeals(getUser(userName));
    return JsonStream.serialize(providers);
  }

  @PostMapping("/setUserPreferences")
  public ResponseEntity<UserPreferences> setUserPreferences(@RequestParam String userName, @RequestBody UserSetterPreferencesDto userSetterPreferencesDto) {
    if (userSetterPreferencesDto == null){
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(tourGuideService.setUserPreferences(userName, userSetterPreferencesDto));
  }


  private User getUser(String userName) {
    return tourGuideService.getUser(userName);
  }


}