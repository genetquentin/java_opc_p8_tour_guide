package tourGuide.proxy.tripPricer;

import java.util.UUID;

public class ProviderProxy {

  public final String name;
  public final double price;
  public final UUID tripId;

  public ProviderProxy(UUID tripId, String name, double price) {
    this.name = name;
    this.tripId = tripId;
    this.price = price;
  }
}
