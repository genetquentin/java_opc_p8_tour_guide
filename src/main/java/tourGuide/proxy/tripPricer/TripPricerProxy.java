package tourGuide.proxy.tripPricer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TripPricerProxy {

  @Autowired
  private TripPricerClient tripPricerClient;
/*
  public TripPricerProxy() {
    this.tripPricer = new TripPricer();
  }
*/

  public List<ProviderProxy> getPrice(String tripPricerApiKey, UUID userId, int numberOfAdults, int numberOfChildren, int tripDuration, int cumulatativeRewardPoints) {
    List<ProviderProxy> providerProxyList = tripPricerClient.getPrice(tripPricerApiKey, userId, numberOfAdults, numberOfChildren, tripDuration, cumulatativeRewardPoints).stream()
        .map(provider -> new ProviderProxy(provider.tripId, provider.name, provider.price))
        .collect(Collectors.toList());
    return providerProxyList;
  }
}