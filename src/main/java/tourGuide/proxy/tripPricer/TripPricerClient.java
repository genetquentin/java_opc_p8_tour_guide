package tourGuide.proxy.tripPricer;

import feign.Param;
import feign.RequestLine;

import java.util.List;
import java.util.UUID;

public interface TripPricerClient {

  @RequestLine("GET /prices?tripPricerApiKey={tripPricerApiKey}?userId={userId}?numberOfAdults={numberOfAdults}?numberOfChildren={numberOfChildren}?tripDuration={tripDuration}?cumulatativeRewardPoints={cumulatativeRewardPoints}")
  List<ProviderProxy> getPrice(@Param("tripPricerApiKey") String tripPricerApiKey,
                               @Param("userId") UUID userId,
                               @Param("numberOfAdults") int numberOfAdults,
                               @Param("numberOfChildren") int numberOfChildren,
                               @Param("tripDuration") int tripDuration,
                               @Param("cumulatativeRewardPoints") int cumulatativeRewardPoints);

}
