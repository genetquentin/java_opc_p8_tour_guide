package tourGuide.proxy.rewardCentral;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RewardCentralProxy {

  @Autowired
  private RewardCentralClient rewardCentralClient;

  /*
  private final RewardCentral rewardCentral;

  public RewardCentralProxy() {
    this.rewardCentral = new RewardCentral();
  }
*/
  //Je pense avoir modifié par erreur les parametre de cette methode par rapport au code d'origine du projet initial (dans une autre classe)
  public int getAttractionRewardPoints(UUID attractionId, UUID userId) {
    return rewardCentralClient.getAttractionRewardPoints(attractionId, userId);
  }
}
