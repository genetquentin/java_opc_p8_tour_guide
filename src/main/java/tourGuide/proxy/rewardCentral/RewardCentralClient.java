package tourGuide.proxy.rewardCentral;


import feign.Param;
import feign.RequestLine;

import java.util.UUID;

public interface RewardCentralClient {

  @RequestLine("GET /attractionRewardPoints?attractionId={attractionId}&userId={userId}")
  int getAttractionRewardPoints(@Param("attractionId") UUID attractionId, @Param("userId") UUID userId);
}
