package tourGuide.proxy.gps;

import feign.Param;
import feign.RequestLine;

import java.util.List;
import java.util.UUID;

public interface GpsClient {

  @RequestLine("GET /attractions")
  List<AttractionProxy> getAttractions();

  @RequestLine("GET /locations?userId={userId}")
  VisitedLocationProxy getUserLocation(@Param("userId")UUID userId);
}
