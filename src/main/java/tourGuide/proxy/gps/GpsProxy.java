package tourGuide.proxy.gps;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class GpsProxy {

  @Autowired
  private GpsClient gpsClient;

  public List<AttractionProxy> getAttractions(){
    return gpsClient.getAttractions();
  }

  public VisitedLocationProxy getUserLocation(UUID userId) {
    return gpsClient.getUserLocation(userId);
  }
}
