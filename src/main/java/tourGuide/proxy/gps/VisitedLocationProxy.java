package tourGuide.proxy.gps;

import java.util.Date;
import java.util.UUID;

public class VisitedLocationProxy {
  public final UUID userId;
  public final LocationProxy location;
  public final Date timeVisited;

  public VisitedLocationProxy(UUID userId, LocationProxy location, Date timeVisited) {
    this.userId = userId;
    this.location = location;
    this.timeVisited = timeVisited;
  }
}
