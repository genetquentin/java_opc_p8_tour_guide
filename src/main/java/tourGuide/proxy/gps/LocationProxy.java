package tourGuide.proxy.gps;

public class LocationProxy {

  public final double longitude;
  public final double latitude;

  public LocationProxy(double latitude, double longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

}
