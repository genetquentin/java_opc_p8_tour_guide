package tourGuide.configuration;

import java.util.Locale;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



/**
 * 'LocaleSetter' is a Bean scanned at the beginning when running application.
 * It let to replace ',' by '.' with float and double numbers.
 */
@Configuration
public class LocaleSetter {

  /**
   * This method is scanned at the beginning when running application
   * to replace each ',' by ' '.' for float or double numbers.
   *
   * @return Locale set to 'US'.
   */
  @Bean
  public Locale setterForLocale() {
    Locale.setDefault(Locale.US);
    return Locale.getDefault();
  }

}
