package tourGuide.configuration;

import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import tourGuide.proxy.gps.GpsClient;
import tourGuide.proxy.gps.GpsProxy;
import tourGuide.proxy.rewardCentral.RewardCentralClient;
import tourGuide.proxy.rewardCentral.RewardCentralProxy;
import tourGuide.proxy.tripPricer.TripPricerClient;
import tourGuide.service.RewardsService;

@Configuration
public class TourGuideModule {

  @Value("${gps-url}")
  String gpsUrl;

  @Value("${rewards-url}")
  String rewardsUrl;

  @Value("${trippricer-url}")
  String trippricer;


  @Primary
  @Bean
  public RewardsService getRewardsService() {
    return new RewardsService(getGpsProxy(), getRewardCentralProxy());
  }

  @Primary
  @Bean
  public GpsProxy getGpsProxy() {
    return new GpsProxy();
  }

  @Primary
  @Bean
  public RewardCentralProxy getRewardCentralProxy() {
    return new RewardCentralProxy();
  }

  @Bean
  public GpsClient getGpsClient() {
    return Feign.builder()
        .client(new OkHttpClient())
        .encoder(new GsonEncoder())
        .decoder(new GsonDecoder())
        .target(GpsClient.class, gpsUrl);
  }

  @Bean
  public RewardCentralClient getRewardCentralClient() {
    return Feign.builder()
        .client(new OkHttpClient())
        .encoder(new GsonEncoder())
        .decoder(new GsonDecoder())
        .target(RewardCentralClient.class, rewardsUrl);

  }

  @Bean
  public TripPricerClient getTripPricerClient() {
    return Feign.builder()
        .client(new OkHttpClient())
        .encoder(new GsonEncoder())
        .decoder(new GsonDecoder())
        .target(TripPricerClient.class, trippricer);
  }
}
