package tourGuide.user;

import tourGuide.proxy.gps.AttractionProxy;
import tourGuide.proxy.gps.VisitedLocationProxy;

public class UserReward {

	public final VisitedLocationProxy visitedLocation;

	public final AttractionProxy attraction;

	private int rewardPoints;

	public UserReward(VisitedLocationProxy visitedLocation, AttractionProxy attraction, int rewardPoints) {
		this.visitedLocation = visitedLocation;
		this.attraction = attraction;
		this.rewardPoints = rewardPoints;
	}
	
	public UserReward(VisitedLocationProxy visitedLocation, AttractionProxy attraction) {
		this.visitedLocation = visitedLocation;
		this.attraction = attraction;
	}

	public void setRewardPoints(int rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	
	public int getRewardPoints() {
		return rewardPoints;
	}
	
}
