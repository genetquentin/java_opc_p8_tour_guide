package tourGuide;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import tourGuide.service.TourGuideService;
import tourGuide.tracker.Tracker;

@Component
public class GpsStarter implements CommandLineRunner {

  private Logger logger = LoggerFactory.getLogger(GpsStarter.class);

  private static final int USER_NUMBER = 10000;

  @Autowired
  private TourGuideService tourGuideService;

  @Override
  public void run(String... args) throws Exception {
    boolean testMode = true;

    if (testMode) {
      logger.info("TestMode enabled");
      logger.debug("Initializing users");
      tourGuideService.initializeInternalUsers(USER_NUMBER);
      logger.debug("Finished initializing users");
    }
    Tracker tracker = new Tracker(tourGuideService);
    Runtime.getRuntime().addShutdownHook(new Thread() {
      public void run() {
        tracker.stopTracking();
      }
    });
  }
}
