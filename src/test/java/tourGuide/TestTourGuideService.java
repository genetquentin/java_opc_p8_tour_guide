package tourGuide;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tourGuide.dto.NearByAttractionsDto;
import tourGuide.helper.InternalTestHelper;
import tourGuide.proxy.gps.GpsProxy;
import tourGuide.proxy.gps.VisitedLocationProxy;
import tourGuide.proxy.rewardCentral.RewardCentralProxy;
import tourGuide.proxy.tripPricer.ProviderProxy;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.user.User;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestTourGuideService {

	//MOCKER LES APPELS EXTERNES
	@Ignore
	@Test
	public void getUserLocation() {
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocationProxy visitedLocation = tourGuideService.trackUserLocation(user);
//		tourGuideService.tracker.stopTracking();
		assertTrue(visitedLocation.userId.equals(user.getUserId()));
	}

	@Ignore
	@Test
	public void addUser() {
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		tourGuideService.addUser(user);
		tourGuideService.addUser(user2);
		
		User retrivedUser = tourGuideService.getUser(user.getUserName());
		User retrivedUser2 = tourGuideService.getUser(user2.getUserName());
		
		assertEquals(user, retrivedUser);
		assertEquals(user2, retrivedUser2);
	}

	@Ignore
	@Test
	public void getAllUsers() {
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		tourGuideService.addUser(user);
		tourGuideService.addUser(user2);
		
		List<User> allUsers = tourGuideService.getAllUsers();
		
		assertTrue(allUsers.contains(user));
		assertTrue(allUsers.contains(user2));
	}

	@Ignore
	@Test
	public void trackUser() {
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocationProxy visitedLocation = tourGuideService.trackUserLocation(user);
		
		assertEquals(user.getUserId(), visitedLocation.userId);
	}
	
	@Ignore // Not yet implemented
	@Test
	public void getNearbyAttractions() {
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocationProxy visitedLocation = tourGuideService.trackUserLocation(user);
		
		List<NearByAttractionsDto> attractions = tourGuideService.getNearByAttractions(visitedLocation);
		
		assertEquals(5, attractions.size());
	}

	@Ignore
	@Test
	public void getTripDeals() {
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

		List<ProviderProxy> providers = tourGuideService.getTripDeals(user);
		
		assertEquals(10, providers.size());
	}
	
	
}
