package tourGuide;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tourGuide.helper.InternalTestHelper;
import tourGuide.proxy.gps.AttractionProxy;
import tourGuide.proxy.gps.GpsProxy;
import tourGuide.proxy.gps.VisitedLocationProxy;
import tourGuide.proxy.rewardCentral.RewardCentralProxy;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.user.User;
import tourGuide.user.UserReward;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestRewardsService {

	@Ignore
	@Test
	public void userGetRewards() {
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());

		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		AttractionProxy attraction = gpsProxy.getAttractions().get(0);
		user.addToVisitedLocations(new VisitedLocationProxy(user.getUserId(), attraction, new Date()));
		tourGuideService.trackUserLocation(user);
		List<UserReward> userRewards = user.getUserRewards();
		assertEquals(1, userRewards.size());
	}

	@Ignore
	@Test
	public void isWithinAttractionProximity() {
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		AttractionProxy attraction = gpsProxy.getAttractions().get(0);
		assertTrue(rewardsService.isWithinAttractionProximity(attraction, attraction));
	}
	
 // Needs fixed - can throw ConcurrentModificationException
	@Ignore
	@Test
	public void nearAllAttractions() {
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		rewardsService.setProximityBuffer(Integer.MAX_VALUE);

		InternalTestHelper.setInternalUserNumber(1);
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		
		rewardsService.calculateRewards(tourGuideService.getAllUsers().get(0));
		List<UserReward> userRewards = tourGuideService.getUserRewards(tourGuideService.getAllUsers().get(0));

		assertEquals(gpsProxy.getAttractions().size(), userRewards.size());
	}
	
}
