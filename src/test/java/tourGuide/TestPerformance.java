package tourGuide;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tourGuide.helper.InternalTestHelper;
import tourGuide.proxy.gps.AttractionProxy;
import tourGuide.proxy.gps.GpsProxy;
import tourGuide.proxy.gps.VisitedLocationProxy;
import tourGuide.proxy.rewardCentral.RewardCentralProxy;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.tracker.TrackingTask;
import tourGuide.user.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class TestPerformance {
	
	/*
	 * A note on performance improvements:
	 *     
	 *     The number of users generated for the high volume tests can be easily adjusted via this method:
	 *     
	 *     		InternalTestHelper.setInternalUserNumber(100000);
	 *     
	 *     
	 *     These tests can be modified to suit new solutions, just as long as the performance metrics
	 *     at the end of the tests remains consistent. 
	 * 
	 *     These are performance metrics that we are trying to hit:
	 *     
	 *     highVolumeTrackLocation: 100,000 users within 15 minutes:
	 *     		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     *
     *     highVolumeGetRewards: 100,000 users within 20 minutes:
	 *          assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	 */

	
	@Test
	public void highVolumeTrackLocationFor100Users() {
		//Given
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService( gpsProxy, new RewardCentralProxy());
		// Users should be incremented up to 100,000, and test finishes within 15 minutes
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		tourGuideService.initializeInternalUsers(100);
		TrackingTask trackingTask = new TrackingTask(tourGuideService);

		//when
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		trackingTask.doTracking(tourGuideService.getAllUsers());
		stopWatch.stop();

		//then
		System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}

	@Test
	public void highVolumeTrackLocationFor1000Users() {
		//Given
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		// Users should be incremented up to 100,000, and test finishes within 15 minutes
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		tourGuideService.initializeInternalUsers(1000);
		TrackingTask trackingTask = new TrackingTask(tourGuideService);

		//when
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		trackingTask.doTracking(tourGuideService.getAllUsers());
		stopWatch.stop();

		//then
		System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}


	@Test
	public void highVolumeTrackLocationFor5000Users() {
		//Given
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		// Users should be incremented up to 100,000, and test finishes within 15 minutes
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		tourGuideService.initializeInternalUsers(5000);
		TrackingTask trackingTask = new TrackingTask(tourGuideService);

		//when
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		trackingTask.doTracking(tourGuideService.getAllUsers());
		stopWatch.stop();

		//then
		System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}


	@Test
	public void highVolumeTrackLocationFor10000Users() {
		//Given
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		// Users should be incremented up to 100,000, and test finishes within 15 minutes
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		tourGuideService.initializeInternalUsers(10000);
		TrackingTask trackingTask = new TrackingTask(tourGuideService);

		//when
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		trackingTask.doTracking(tourGuideService.getAllUsers());
		stopWatch.stop();

		//then
		System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}

	@Test
	public void highVolumeTrackLocationFor50000Users() {
		//Given
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		// Users should be incremented up to 100,000, and test finishes within 15 minutes
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		tourGuideService.initializeInternalUsers(50000);
		TrackingTask trackingTask = new TrackingTask(tourGuideService);

		//when
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		trackingTask.doTracking(tourGuideService.getAllUsers());
		stopWatch.stop();

		//then
		System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}

	@Test
	public void highVolumeTrackLocationFor100000Users() {
		//Given
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());
		// Users should be incremented up to 100,000, and test finishes within 15 minutes
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		tourGuideService.initializeInternalUsers(100000);
		TrackingTask trackingTask = new TrackingTask(tourGuideService);

		//when
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		trackingTask.doTracking(tourGuideService.getAllUsers());
		stopWatch.stop();

		//then
		System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds."); 
		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}

	@Ignore
	@Test
	public void highVolumeGetRewards() {
		GpsProxy gpsProxy = new GpsProxy();
		RewardsService rewardsService = new RewardsService(gpsProxy, new RewardCentralProxy());

		// Users should be incremented up to 100,000, and test finishes within 20 minutes
		InternalTestHelper.setInternalUserNumber(100);
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		TourGuideService tourGuideService = new TourGuideService(gpsProxy, rewardsService);
		
	    AttractionProxy attraction = gpsProxy.getAttractions().get(0);
		List<User> allUsers = new ArrayList<>();
		allUsers = tourGuideService.getAllUsers();
		allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocationProxy(u.getUserId(), attraction, new Date())));
	     
	    allUsers.forEach(u -> rewardsService.calculateRewards(u));
	    
		for(User user : allUsers) {
			assertTrue(user.getUserRewards().size() > 0);
		}
		stopWatch.stop();

		System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds."); 
		assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}
	
}
