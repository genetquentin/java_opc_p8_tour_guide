FROM openjdk:8-alpine
COPY build/libs/tourGuide-1.0.0.jar tourguide-main.jar
CMD ["java", "-jar", "-Dspring.profiles.active=docker", "tourguide-main.jar"]

